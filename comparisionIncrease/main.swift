//
//  main.swift
//  comparisionIncrease
//
//  Created by vikingosegundo on 28/11/2022.
//

import Foundation

struct ImmutableNumber {
    enum Change {
        case increase
    }
    
    func alter(_ c:Change) -> Self {
        switch c {
        case .increase: return Self(value:value+1)
        }
    }
    let value: Int
}

final class MutableNumber {
    init(value v:Int) {
        value = v
    }
    var value:Int
    func increment() {
        value = value + 1
    }
}

let n = 5_000_000
let r = 10
for _ in 0 ..< r {
    var inumber = ImmutableNumber(value:0)
    let mnumber = MutableNumber(value:0)
    var d00:Date!
    var d01:Date!
    var d10:Date!
    var d11:Date!
    
    func muTest() { d10 = Date(); (0 ..< n).forEach { _ in mnumber.increment()                }; d11 = Date() }
    func imTest() { d00 = Date(); (0 ..< n).forEach { _ in inumber = inumber.alter(.increase) }; d01 = Date()}
    let test = (imTest,muTest)
    if Bool.random() {
        test.1()
        test.0()
    } else {
        test.0()
        test.1()
    }
    
    print("expected value: \((mnumber.value == inumber.value) == (n == inumber.value) )")
    print("immuable: \(inumber.value) \(d01.timeIntervalSince1970 - d00.timeIntervalSince1970) \t \(((d01.timeIntervalSince1970 - d00.timeIntervalSince1970) / Double(n)) * 1_000_000_000)\tns/step")
    print(" mutable: \(mnumber.value) \(d11.timeIntervalSince1970 - d10.timeIntervalSince1970) \t \(((d11.timeIntervalSince1970 - d10.timeIntervalSince1970) / Double(n)) * 1_000_000_000)\tns/step")
    print("")
}
